
FROM ubuntu

ENV PATH="$PATH:/flutter/bin"
ENV CHROME_EXECUTABLE=/usr/bin/chromium-browser
ARG VERSION

RUN apt update
RUN apt install -y wget tar xz-utils git
RUN wget https://storage.googleapis.com/flutter_infra_release/releases/stable/linux/flutter_linux_$VERSION-stable.tar.xz -O flutter.tar.xz
RUN tar xf flutter.tar.xz
RUN rm flutter.tar.xz

RUN flutter doctor
